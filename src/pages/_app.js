import '../../styles/styles.global.scss';
import '../../styles/component.scss';

export default function Index ({ Component, defaultProps }) {
    return (
        <Component {...defaultProps} />
    )
} 