import Document, { Head, Main, NextScript } from 'next/document';
import flush from 'styled-jsx/server';

export default class CustomDocument extends Document {
    static getInitialProps({ renderPage }) {
        const { html, head, errorHtml, chunks } = renderPage();
        const styles = flush();

        return { html, head, errorHtml, chunks, styles };
    }

    render() {
        return (
            <html>
                <Head>
                    <link rel="StyleSheet" href="/static/lib/bootstrap.min.css" />
                    <script src="/static/lib/jquery.min.js"></script>
                    <script src="/static/lib/bootstrap.min.js"></script>
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </html>
        );
    }
}