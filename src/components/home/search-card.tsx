import { Button } from '../shared/button';

interface ISearchCardProps {
    placeholder?: string;
    onSearchClick?: () => void;
}

export const SearchCard = (props: ISearchCardProps) => {
    return (
        <div className="search-card">
            <div className="input">
                <input placeholder={props.placeholder}/> 
            </div>
            <img src={`/static/images/search/location.svg`}/>
            <div className="select">
                <select></select>
            </div>
            <div className="button">
                <Button label="SEARCH NOW" onClick={ () => props.onSearchClick 
                && props.onSearchClick()} />
            </div>
        </div>
    )
}